from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from cards.models import Cards
import json
from django.core.serializers.json import DjangoJSONEncoder
from django.core import serializers
from django.shortcuts import render_to_response
from django.template import RequestContext
from forms import testForm

def index(request):
    cardss = Cards.objects.all()
    data = [ obj.as_json() for obj in cardss]
    return HttpResponse(json.dumps({"data": data}), content_type='application/json')

def editcards(request):
    return HttpResponse("")

def newcards(request):
    json = {'title': 0,
            'series': 0,
            'number': 3,
            'release_date': 4,
            'finish_date': 4,
            'date_last_use': 4,
            'cash': 4,
            'status': 4}
    form = testForm(request.POST or None, initial={'data': json})
    if form.is_valid():
        # validate and save
        pass

    template = 'cardstemplate/test_template.html'
    context = RequestContext(request, {'form': form})
    return render_to_response(template, context)
