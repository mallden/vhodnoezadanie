from django.db import models

class Cards(models.Model):
    title = models.CharField(max_length=50)
    series = models.CharField(max_length=4)
    number = models.CharField(max_length=12)
    release_date = models.DateField()
    finish_date = models.DateField()
    date_last_use = models.DateField()
    cash = models.IntegerField()
    noneactive = 'NA'
    active = 'AC'
    prosroch = 'DE'
    status_cards = (
    (noneactive, 'None Active'),
    (active, 'Active'),
    (prosroch, 'Delay'),
    )
    status = models.CharField(max_length=2, choices = status_cards,default=active)

    def as_json(self):
        return {
            "id": self.id,
            "title" : self.title,
            "series" : self.series,
            "number" : self.number,
            "release_date" : self.release_date.strftime('%d.%m.%Y'),
            "finish_date" : self.finish_date.strftime('%d.%m.%Y'),
            "status" : self.status
        }

    def __unicode__(self):
        return self.title
