from django.conf.urls import url, include
from django.views.generic import ListView, DetailView
from cards.models import Cards
from django.core.serializers import serialize
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^newcards', views.test_dict, name='newcards'),
]
